# List of models

## Kontor
* Vinduer
* Skrivebordslampe
* Reolindhold
* Symaskiner
* Kontorstole
* Prusa printer
* ~~hulplade~~
* ~~loftlampe~~
* ~~reol2m\_skuffer~~
* ~~reolvaeg~~
* ~~skrivebord~~
* ~~spejl~~
* ~~sybord~~
* ~~symaskinehylde~~
* ~~syskuffer~~

## Stue
* Radiator 2
* Vinduer
* Gardiner
* Loft-kasse
* Reoler
* Bøger
* Brændeovn
* Brændekasse
* Pejseværktøj
* Sofa
* Puf
* Spisebordslampe
* Hjørnelampe
* Stuebordslampe
* Rund lampe
* ~~Bjælke~~
* ~~Skænk~~
* ~~Sofabord~~
* ~~Spisebord~~
* ~~Spisestue_stol~~
* ~~Stuebilleder~~
* ~~TV reol~~
* ~~Ur~~
* ~~Radiator 1~~

## Køkken
* Underskabe
* Overskabe
* Bordplade
* Vaskemaskine
* Vandhane+vask
* Komfur
* Ovn
* Emhætte
* Vindue
* Te-hylder
* Ophængningsstænger
* Hundeskåle
* Køleskab

## Gang
* Gangskabe
* Billeder
* Gul dør
* Vasketøjskasser
* Reoler
* Bennoreoler
* Skoreol

## Badeværelse
* Skab1
* Skab2
* Vask
* Afløb
* Håndlædestang
* Bruseforhæng
* Badearmatur
* Flisevæg
* Badeværelsesdør
* ~~Badekar~~
* ~~Kasse~~

## Soveværelse
* Klædeskab
* Loftshylde
* Vægskabe
* Seng
* Sengelamper
* Natbord
* Gardiner

## Victoria
* Seng
* Reoler
* Loftshylde
* PH lampe
* Vægkasser
* Foldebord
* Symfonisk
* Gardiner

## Alexander
* Seng
* Sengekasse
* Reol
* Computerbord
* ~~Tøjreol~~
* Loftshylde
* Gardiner

## Bryggers
* Reoler
* Fyr
* Tørrestativ
* Tørretumbler
* Vaskemaskine
* Bordplade
* Kontakter??

## Udenfor
* Bil
* Havegangslamper
* Indgangsparti
* Brændeskur ???
* Garage ???
* Havebord ???
* Havestole ???
* Grill ???
* Buske ???
* Fryser
* Brændeskur-hylder
* ~~Havehegn~~
