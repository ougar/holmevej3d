H=50;
w=10;

hus_y=600;
kontor_x=400;
kontor_y=400;
stue_x=600;
stue_x1=300;
vaerelse_x=300;
vaerelse_y=300;
kokken_x=stue_x-stue_x1;
kokken_y=300;
gang_x=200;
bade_x=300;
bade_y=180;
mellem_x=200;
bryggers_x=400;
bryggers_y=300;

skab_x=400;
skab_y=50;
elskab_x=70;
elskab_y=35;

module hwall(x,y,l,d=w) {
    translate([x+l/2,y,H/2])
        cube([l+d,d,H],center=true);
}
module vwall(x,y,l) {
    translate([x,y+l/2,H/2])
        cube([w,w+l,H],center=true);
}

// Outer wall
vwall(0,hus_y-kontor_y,kontor_y);
hwall(0,hus_y-kontor_y,kontor_y);
vwall(kontor_x,0,hus_y-kontor_y);
hwall(kontor_x,0,stue_x+3*vaerelse_x);
vwall(kontor_x+stue_x+3*vaerelse_x,0,vaerelse_y);
hwall(kontor_x+stue_x+3*vaerelse_x,vaerelse_y,bryggers_x/2);
vwall(kontor_x+stue_x+3*vaerelse_x+bryggers_x/2,vaerelse_y,bryggers_y);
hwall(0,hus_y,kontor_x+stue_x1+kokken_x+gang_x+bade_x+mellem_x+bryggers_x);

// Stue
vwall(kontor_x,hus_y-kontor_y,kontor_y);
vwall(kontor_x+stue_x1,hus_y-kokken_y,kokken_y);
vwall(kontor_x+stue_x,0,hus_y);
   
// Værelser
hwall(kontor_x+stue_x,vaerelse_y,3*vaerelse_x);
vwall(kontor_x+stue_x+vaerelse_x,0,vaerelse_y);
vwall(kontor_x+stue_x+2*vaerelse_x,0,vaerelse_y);

// Gang
hwall(kontor_x+stue_x+gang_x,hus_y-bade_y,bade_x);
vwall(kontor_x+stue_x+gang_x,hus_y-bade_y,bade_y);
vwall(kontor_x+stue_x+gang_x+bade_x,vaerelse_y,hus_y-vaerelse_y);
vwall(kontor_x+stue_x+gang_x+bade_x+mellem_x,vaerelse_y,hus_y-vaerelse_y);

// Inventar
hwall(kontor_x+stue_x+vaerelse_x-(skab_x-skab_y)/2,vaerelse_y+skab_y/2,skab_x-skab_y,d=skab_y);
hwall(kontor_x+stue_x+gang_x-elskab_x,hus_y-bade_y+(elskab_y-w)/2,elskab_x-elskab_y/2,d=elskab_y);