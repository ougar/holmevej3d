# holmevej3d

Library of 3D models of my house. Typically created in OpenSCAD, exported to .stl files and converted to
obj files either using web service or Blender if I need to combine models to obj-files with multiple
objects.

The models are placed in folders named after which room, they are located in.

Common functionality is located in the common.scad file in the root folder (soon).

## Sweet Home 3D

The models are used to create my Sweet Home 3D model of my house - the exported house model is
kept in the sweethome folder (soon).

## Testing

I would like to integrate tests and automatically "run" the code in OpenSCAD and 
automatically generate the STL files, and automatically convert them to object
files - and generally ensure that the models are kept updated - but I haven't
figured out how to do that.
