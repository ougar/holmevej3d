use <MCAD/boxes.scad>;
use <quooker.scad>;

module vaskhane(parts=[1,1,1], color=false, hane=true) {
    vaskh=30;
    vaskd=50;
    vaskw=53;
    vasky=7+vaskd/2;
    vaskx=-40-60-2*.2-vaskw/2-3.5;
    vaskz=-56-.2;
    vaskr=.8;
    vask_depth=20;

    R2=2;
    R1=2;
    r1=.3;
    dif=0;
    edge=2;
    D=vaskd-2*edge;
    W=vaskw-2*edge;

    topd=8;
    
    afloeb_r1=6;
    afloeb_r2=4;

    module cylcorn(corn) {
        rotate([0,0,90*corn]) {
            translate([-R1,-R1,0])
                rotate_extrude(angle=90, $fn=40)
                    translate([R1,0,0])
                        circle(r1);
            difference() {
                translate([-R1/2+r1/2,-R1/2+r1/2,0]) 
                    cube([R1+r1,R1+r1,2*r1],center=true);
                translate([-R1,-R1,0])
                    cylinder(10,R1,R1,$fn=40,center=true);
            }
        }
    }

    module cyledge() {
        translate([0,R1-dif/2,0]) rotate([-90,0,0]) cylinder(D-2*R1+dif,r1,r1,$fn=40);
        translate([W,R1-dif/2,0]) rotate([-90,0,0]) cylinder(D-2*R1+dif,r1,r1,$fn=40);
        translate([R1-dif/2,0,0]) rotate([0, 90,0]) cylinder(W-2*R1+dif,r1,r1,$fn=40);
        translate([R1-dif/2,D,0]) rotate([0, 90,0]) cylinder(W-2*R1+dif,r1,r1,$fn=40);
        translate([0,0,0]) cylcorn(2);
        translate([0,D,0]) cylcorn(1);
        translate([W,0,0]) cylcorn(3);
        translate([W,D,0]) cylcorn(0);
        translate([vaskw/2-edge,-edge/2,r1-15])  cube([vaskw-2*R2,edge,30],center=true);
        translate([vaskw/2-edge,D+edge/2,r1-15]) cube([vaskw-2*R2,edge,30],center=true);
        translate([-edge/2,vaskd/2-edge,r1-15])  cube([edge,vaskd-2*R2,30],center=true);
        translate([W+edge/2,vaskd/2-edge,r1-15]) cube([edge,vaskd-2*R2,30],center=true);
        translate([0,0,0]) rotate([0,0,0])   corner();
        translate([W,0,0]) rotate([0,0,90])  corner();
        translate([0,D,0]) rotate([0,0,-90]) corner();
        translate([W,D,0]) rotate([0,0,180]) corner();
    }

    module corner() {
        translate([R2-edge,R2-edge,r1-15]) 
            difference() {
                cylinder(30,R2,R2,$fn=40,center=true);
                rotate([0,0,45]) translate([2*R2,0,0]) cube([4*R2,4*R2,40],center=true);
            }
    }


    module vask(parts=[1,1,1], color=false) {
        difference() {
            union() {
                translate([edge-vaskw/2,edge-vaskd/2,-r1]) cyledge();
                translate([0,0,r1/2-15]) cube([W,D,30-2*r1],center=true);
            }
            roundedBox([W-1.7*r1,D-1.7*r1,3],radius=R1-r1,sidesonly=true,$fn=40);
            translate([0,-topd/2,0])
                roundedBox([W-1.7*r1,D-1.7*r1-topd,2*vask_depth],radius=R1-r1,sidesonly=true,$fn=40);
            translate([0,0,-vask_depth-5])
                cylinder(15.5,afloeb_r1,afloeb_r1,$fn=40,center=true);
        }
        afloeb(parts=parts, color=color);
    }
    
    module afloeb_insert() {
        afloeb_r2=3.8;
        afloeb_r3=1.85;
        thick=.1;

        difference() {
            union() {
                r1=afloeb_r2-afloeb_r3;
                rotate_extrude($fn=100)
                    translate([afloeb_r2-afloeb_r3,0,0])
                        circle(afloeb_r3);
                cylinder(2*afloeb_r3,r1,r1,center=true,$fn=20);
            }

            rotate_extrude($fn=40)
                translate([afloeb_r2-afloeb_r3-thick,0,0])
                    circle(afloeb_r3-thick);
            r2=afloeb_r2-afloeb_r3-thick;
            cylinder(2*(afloeb_r3-thick),r2,r2,center=true,$fn=20);    
            translate([0,0,5]) cube([10,10,10],center=true);
            for (a=[0:23]) pinhole(15*a);
        }
        translate([0,0,-(afloeb_r3-thick)])
            cylinder(1.2,.3,.3,$fn=20);
        translate([0,0,-(afloeb_r3-thick)+1.1])
            cylinder(.3,.7,.7,$fn=20);
    }

    module pinhole(angle=0) {
        dhole=1.2;
        rhole=.2;
        movehole=3;
        rotate([0,0,angle]) {
            translate([movehole,0,-1]) {
                rotate([0,-45,0]) {
                    translate([-dhole/2,0,0])
                        cylinder(1,rhole,rhole,$fn=5,center=true);
                    translate([dhole/2,0,0])
                        cylinder(1,rhole,rhole,$fn=5,center=true);
                    cube([dhole,2*rhole,1],center=true);
                }
            }
        }
    }
   
    module afloeb(parts=parts, color=color) {
        translate([0,0,-vask_depth-1])
            difference() {
                cylinder(1,afloeb_r1,afloeb_r1,$fn=40);
                cylinder(3,afloeb_r1-.2,afloeb_r1-.2,center=true,$fn=40);
            }
        translate([0,0,-vask_depth-1])
            color("black") difference() {
                cylinder(1,afloeb_r1,afloeb_r1,$fn=40);
                cylinder(3,afloeb_r1-.2,afloeb_r1-.2,center=true,$fn=40);
            }
        translate([0,0,-vask_depth-1.1])
            difference() {
                cylinder(1,afloeb_r1-.2,afloeb_r1-.2,$fn=40);
                translate([0,0,.01])
                    cylinder(1,.1,afloeb_r1-.2,$fn=40);
                cylinder(5,afloeb_r2,afloeb_r2,$fn=40,center=true);
            }
        translate([0,0,-vask_depth-.5]) afloeb_insert();
    }
    
    if (color) { color(color) vask(parts=parts, color=color); }
    else vask(parts=parts, color=color);
        
    if (hane) {
        translate([0,D/2-topd/2,-1.6])
            rotate([0,0,-90])
                quooker(parts=parts, color=color);
    }
}

module vaskindbyg(parts=[1,1,1], color=false) {
    vaskd=50;
    vaskw=53;
    vasky=7+vaskd/2;
    vaskx=-40-60-2*.2-vaskw/2-3.5;
    vaskz=-56-.2;
    translate([vaskx,vasky,vaskz]) {
        rotate([0,0,180]) {
            vaskhane(parts=parts,color=color);
        }
    }
}

vaskhane(hane=false);
//vaskindbyg();