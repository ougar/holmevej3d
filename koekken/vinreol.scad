D=2.8;
Dcut=3.5;
L=25;

s=9.8;
d=1.1;
P=9.5;

n=15;


module stick() {
    intersection() {
        rotate([0,45,0])
            cube([D,L,D],center=true);
        cube([Dcut,L,Dcut],center=true);
    }
}

for (x=[0:n]) {
    translate([P*x,0,0])
        stick();
    if (x>0) {
        pins(x-1);
    }
}

module pins(n) {
    translate([n*P,0,0])
        for (q=[-1,1])
            translate([0,q*s,0])
                rotate([0,90,0])
                    cylinder(P,d/2,d/2,$fn=20);
}