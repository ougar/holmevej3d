white=true;
metal=true;
black=true;
glass=true;

module kogeplader() {
    h1=.4;
    h2=.7;
    D=50;
    W=60;
    
    angle=10;
    qq1=tan(angle)*W/2;
    qq2=tan(angle)*D/2;
    
    //color("black")
    difference() {
        cube([W,D,h2],center=true);
    
        rotate([  0,-10, 0]) translate([0,0,qq1+h1]) cube([1.1*W,1.1*D,h2],center=true);
        rotate([  0, 10, 0]) translate([0,0,qq1+h1]) cube([1.1*W,1.1*D,h2],center=true);
        rotate([ 10,  0, 0]) translate([0,0,qq2+h1]) cube([1.1*W,1.1*D,h2],center=true);
        rotate([-10,  0, 0]) translate([0,0,qq2+h1]) cube([1.1*W,1.1*D,h2],center=true);
    }
}


module ovn() {
    
    
    
}

module mikroovn() {
    
    hbar=35.5;
    
    module button(x,h=.1,y=0,size=.5) {
        translate([30-x,0,hbar])
            rotate([90,0,0]) {
                color("black") cylinder(0.01,size+.1,size+.1,$fn=20);
                color("gray") cylinder(h,size,size,$fn=20);
            }
    }
    
    module buttonb(x,y=0,size=.4) {
        translate([30-x,0,hbar+y])
            rotate([90,0,0]) {
                color("gray")  cylinder(0.11,size+.1,size+.1,$fn=20);
                color("black") cylinder(.2,size,size,$fn=20);
            }
    }
    
    module display() {
        cube([13,.2,3]);
    }
    
    if (white) {
        // Outside
        color("white") 
            difference() {
                translate([0,1,0]) cube([60,35,40]);
                translate([2,-1,2]) cube([56,34,36]);
            }
    }
    if (black) color("#222222") {
        // "Backplate"
        translate([.1,.9,29]) cube([59.8,.2,2]);
        
        // black-front
        difference() {
            translate([2.5,.2,2.5]) cube([55,.1,30-2.5]);
            // Holes
            for (y=[0:20])
                translate([2.5+13,.1,2.5+7+y/20*13.5]) cube([29,1,.6]);
            //translate([2.5+13,.1,2.5+7]) cube([29,1,13.5]);
        }
        translate([(60-13)/2,-.1,34]) display();
    }
    if (metal) color("gray") {
        // Inside oven
        difference() {
            translate([2,1,2]) cube([56,34,36]);
            translate([4,-1,4]) cube([52,33,32]);
        }

        // Metal top
            translate([0,0,31]) cube([60,1,9]);
        // Metal edge
        difference() {
            cube([60,1,30]);
            translate([2.5,-.1,2.5]) cube([55,1.2,30]);
        }
    }
    button(13/2+1.5);
    button(13/2+4);
    button(-13/2-1.5);
    button(-13/2-4);
    button(-22,size=1.5,h=.3);
    
    buttonb(13/2-1,y=.7);

/**/
}
// kogeplader();
// ovn();
mikroovn();