use <MCAD/boxes.scad>;
use <../aalib/skab_skabelon.scad>;
use <../aalib/skab_haandtag.scad>;
use <vask.scad>;


Hloft0=214;
Xloft1=299;
Hloft1=231.5;
Hloftover=Hloft0-90-56;
loftvinkel=atan((Hloft1-Hloft0)/Xloft1);
function lofth(x, overskab=true) = Hloft0+x*(Hloft1-Hloft0)/Xloft1 - (overskab ? (90+56) : 0);

parts=[1,1,1];

T=2;
r=.4;
dopen=.3;
dt=.2;
dx=.2;
D=36;

hy = D+T+dopen+r;
hz = 14;
hx = 5;

skabD=60;
bordD=64;
koelepladeD=62.5;
hbord=90;
htop=56;
tbord=3;
dkoele=5;
coolwidth=61;

footup=6.5;
footin=9;
foot=[footin,footup];

dcorner=9;

x3=dcorner+60+60+60+3*dx;
x2=x3+40;
x1=x2+dkoele+T/2;
x0=x1+coolwidth+T+2*dx;
echo("x0=",x0);

h0=lofth(x0);
h1=lofth(x1);
h2=lofth(x2);
h3=lofth(x3);
module overskabe() {
    bH0=h0+htop+hbord;
    bH1=h1+htop+hbord;

    // Glasskab
    skraa(40,D,[h2,h3],hplace="r",parts=parts);

    // Mikroovnsskab
    translate([-40-dx-30,D/2,40+10]) rotate([0,180,0]) 
        hskab(60,D,20,hplace="drawer",parts=parts);

    // Overvask-skab
    translate([-100-2*dx-30,D/2,30]) 
        hskab(60,D,60,parts=parts,hplace="ll");
    // Hjørneoverskab
    translate([-160-3*dx-30,D/2,30])
        hskab(60,D,60,parts=parts,hplace="ll");

    // Øvre afstandsfætter
    //cube([dkoele,D,81]);
    translate([dkoele,0,0]) skraa(dkoele,D,[h1,h2],parts=[parts[0],0,0]);

    // Køleskabsplader
    if (parts[1]) {
        translate([T/2+dkoele,koelepladeD/2,-bH1/2+h1])
            roundedBox([T,koelepladeD,bH1], .3, $fn=20);
        translate([T/2+T+dkoele+coolwidth+2*dx,koelepladeD/2,-bH0/2+h0]) 
            roundedBox([T,koelepladeD,bH0], .3, $fn=20);
    }

    // Køleskabsskab
    thafst=203-htop-hbord;
    translate([dkoele+T+dx+coolwidth,0,thafst])
    skraa(coolwidth,60,[h0-thafst,h1-thafst],parts=parts,hplace="r");
}

module underskabe() {

    // Bund-afstandsfætter
    if (parts[0]) {
        translate([2.5,30,(-87+footup)/2-htop-tbord])
            cube([5,60,87-footup],center=true);
        translate([0,0,-56-90]) 
            cube([5,60-footin,footup]);
    }

    // Bestikskuffer
    translate([-20,30,-87/2-htop-tbord])
        hskab(40,skabD,87,N=[.25,.25,.25,.25],foot=foot,parts=parts,hplace="drawer");

    // Skraldespandsskab
    difference() {
        translate([-40-60-30-2*dx,30,-87/2-htop-tbord])
            hskab(60,skabD,87,T=T,r=r,dt=dt,N=[1],parts=parts, foot=foot,hplace="ul");
        vaskhul();
    }

    // Hjørnekasse1
    translate([-40-60-60-10-3*dx,30,-87/2-htop-tbord])
        skab(20,skabD,87,T=T,r=r,dt=dt,N=[1],foot=foot,parts=[parts[0],0,0]);

    // Hjørnekasse2
    translate([-30-40-60-60-3*dx-dcorner,40,-87/2-htop-tbord])
        rotate([0,0,-90]) 
            skab(80,skabD,87,T=T,r=r,dt=dt,N=[1],foot=foot, parts=[parts[0],0,0]);
}
        
module bordplade() {
    // bd, tbord;
    bwidth1=dkoele+40+60+60+dcorner+60+3*dx;
    color("#505050")
    difference() {
        translate([dkoele-bwidth1,0,-htop-tbord]) {
            cube([bwidth1,bordD,tbord]);
            cube([bordD,80,tbord]);
        }
        vaskhul();
    }
}

module vaskhul() {
    vaskh=60;
    vaskd=50;
    vaskw=53;
    vasky=7+vaskd/2;
    vaskx=-40-60-2*dx-vaskw/2-3.5;
    vaskz=-htop-vaskh/2+.01;
    vaskr=2;
    translate([vaskx,vasky,vaskz])
        roundedBox([vaskw,vaskd,vaskh],radius=vaskr, sidesonly=true,$fn=40);
}

overskabe();
underskabe();

bordplade();
vaskindbyg(parts=[1,1,1],color="#c0c0c0");