use <MCAD/boxes.scad>;
use <../aalib/skab_skabelon.scad>;
use <../aalib/skab_haandtag.scad>;

parts=[1,1,1]; // Skab, låge, håndtag;

T=2;
r=.4;
dopen=.3;
dt=.2;
dx=.2;

D=60;

hy = D+T+dopen+r;
hz = 14;
hx = 5;

bD=62;
hbord=90;
htop=56;
tbord=3;
H=hbord-tbord;

foot=[9,6.5];
skuffe=(H-foot[1])/4;

dy=D/2+T+dt;
Wcorn=25;

Hloft0=214;
Xloft1=299;
Hloft1=231.5;
Hloftover=Hloft0-90-56;
loftvinkel=atan((Hloft1-Hloft0)/Xloft1);
function lofth(x, overskab=true) = Hloft0+x*(Hloft1-Hloft0)/Xloft1 - (overskab ? (90+56) : 0);

// Test beam;
// translate([-20,0,Hloftover]) rotate([loftvinkel,0,0]) cube([20,200,20]);

module lave_moduler() {    
    // 2 x 80-skuffer
    for (num=[0,1]) {
        translate([(80+dx)*(num)+dx+40+Wcorn,-dy,H/2-tbord])
            hskab(80,D,H,N=[.5,.25,.25],r=r,T=T,dt=dt,foot=foot,parts=parts,hplace="drawer");
    }

    // Lave hjørne 1
    translate([0,-dy,H/2-tbord]) {
        translate([Wcorn/2,0,0])
            skab(Wcorn,D,H,N=[1],r=r,T=T,dt=dt,foot=foot,parts=parts);
        translate([0,0,0])
            skab(40,D,H,foot=foot,parts=[parts[0],0,0]);
    }

    // Lave hjørne 2
    rotate([0,0,-90]) {
        translate([-Wcorn/2,-dy,H/2-tbord])
            hskab(Wcorn,D,H,N=[1],r=r,T=T,dt=dt,foot=foot,hplace="ur",parts=parts);
        translate([0,-dy,H/2-tbord])
            skab(40,D,H,foot=foot,parts=[parts[0],0,0]);
    }

    // Komfur skuffer
    translate([-dy,30+Wcorn+dx,0]) {
        translate([0,0,(foot[1]+10)/2-tbord]) rotate([0,0,-90]) {
            hskab(60,D,foot[1]+10,N=[1],r=r,T=T,dt=dt,foot=foot,hplace="drawer",parts=parts);
        }
        translate([0,0,H-5-tbord]) rotate([0,0,-90]) {
                hskab(60,D,10,N=[1],r=r,T=T,dt=dt,hplace="drawer",parts=parts);
        }
    }

    // Diverse-skuffer
    footend=4;
    translate([-dy,Wcorn+dx+60+dx+20,H/2-tbord]) {
        rotate([0,0,-90]) {
            difference() {
                hskab(40,D,H,N=[.25,.25,.25,.25],r=r,T=T,dt=dt,foot=foot,hplace="drawer",parts=parts);
                translate([-20,0,-H/2])
                    if (parts[0])
                        cube([2*footend,2*D,2*foot[1]],center=true);
            }
        }
    }
    // Endeplade
    if (parts[0])
    translate([-bD/2,Wcorn+dx+60+dx+40+dx+.75,(H+foot[1])/2-tbord]) 
        roundedBox([bD,1.5,H-foot[1]], .3, $fn=20);

} // Nedre moduler

module bordplade() {
    // bd, tbord;
    color("#666666")
    translate([0,0,-htop-tbord]) {
        cube([80+dx+80+dx+Wcorn+bD,bD+2,tbord]);
        cube([bD+2,bD+Wcorn+dx+60+dx+40+dx+2,tbord]);
    }
}

// Flyt hele nedre halvdel
translate([bD,bD,-H-56]) 
    lave_moduler();

bordplade();

// Overskabe
D2=36;
H0=lofth(0);
H1=lofth(D2);
H2=[lofth(D2),lofth(D2+40/sqrt(2))];
H3=[60,60];
correct=2.5;

bagestart=correct+20+D2+40/sqrt(2);
// Endeplade
if (parts[1]) {
    translate([bagestart+10,0,-1])
        rotate([0,0,-90]) 
            skraa(D2+T+r,10,[H0+1,lofth(D2+T+dt)+1],parts=[0,1,0],T=1);
}
// Bageskab
echo("Parts=",parts);
translate([bagestart,D2/2,H1/2]) 
    hskab(40,D2,H1,hplace="lr",parts=[0,parts[1],parts[2]]);
// Hjørneskab
difference() {
    translate([D2+correct,D2+2,0]) rotate([0,0,-45]) 
        translate([20,-D2/2,0]) skraa(40,D2,H2,hplace="l",parts=parts);
    translate([-20,0,Hloftover+.01]) rotate([loftvinkel,0,0]) cube([100,100,20]);
}

// Filler box'es
if (parts[0]) 
    rotate([0,0,-90]) {
        skraa(D2,40+D2+40/sqrt(2)+correct,[H0,H1],parts=[1,0,0]);
        skraa(D2+40/sqrt(2)+correct,D2,[H0,H2[1]],parts=[1,0,0]);
    }

// Opskriftskab
startx=correct+D2+40/sqrt(2);
translate([0,startx,0]) rotate([0,0,-90]) {
    skraa(20,D2,H3,hplace="r",parts=parts);
    translate([-20-dx,0,0]) skraa(60,D2,H3,hplace="r",parts=parts);
    translate([-80-2*dx,0,0]) 
        skraa(40,D2,[lofth(startx+80+2*dx),lofth(startx+120+2*dx)],hplace="l",parts=parts);
    translate([0,0,60+dx]) 
        skraa(80+dx,D2,[lofth(startx)-60-dx,lofth(startx+80)-60-dx],
            parts=[parts[0],parts[1],0]);
}

endeh=lofth(startx+120+2*dx+1.5)+1;
if (parts[1])
translate([D2+T+dx,startx+120+2*dt-10,-1]) 
    skraa(D2+T+dx,10,[endeh,endeh],T=1.5,parts=[0,1,0]);


/* */