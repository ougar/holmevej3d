module quooker(parts=[1,1,1], color=false) {
    R1=2;
    R2=1.15;

    Rhane=11;

    Hfod=8;
    Hmiddle=5;
    Hlys=.3;
    Hknap=2.5;
    Hspace=.1;

    Lhane=10;
    Lende=1;
    Lspids=2.5;

    Rpind=.5;
    Lpind=8.5;
    Hpind=Hfod+.5*Hmiddle;

    len1 = Hfod+Hmiddle+Hlys+Hknap+3*Hspace+Lhane;

    module hane() {
        cylinder(len1,R2,R2,$fn=40);
        translate([Rhane,0,len1])
            rotate([90,0,0])
                rotate_extrude(angle=180,$fn=80)
                    translate([Rhane,0,0])
                        circle(R2);
        translate([2*Rhane,0,len1]) {
            translate([0,0,-Lende])
                cylinder(Lende,R2,R2,$fn=40);
            translate([0,0,-Lende-.1])
                cylinder(.1,R2-.1,R2-.1,$fn=40);
            translate([0,0,-Lende-.1-Lspids]) 
                difference() {
                    cylinder(Lspids,R2,R2,$fn=40);
                    cylinder(20,R2-.2,R2-.2,$fn=40,center=true);
                }
        }
    }

    module fod() {
        cylinder(Hfod,R1,R1,$fn=40);
        translate([0,0,Hfod+Hspace])
            cylinder(Hmiddle,R1,R1,$fn=40);
        translate([0,0,Hfod+Hmiddle+Hlys+3*Hspace]) 
            knap();

        translate([0,0,Hpind])
            rotate([-90,0,0])
                cylinder(Lpind,Rpind,Rpind,$fn=40);
    }
    
    module powerled() {
        translate([0,0,Hfod+Hmiddle+2*Hspace]) 
            color("red") cylinder(Hlys,R1,R1,$fn=40);
    }

    module knap() {
        cylinder(Hknap,R1,R1,$fn=40);
        pins();
    }

    module pins() {
        pi=3.1415926;
        d=.235;
        nangle=round(2*pi*R1/d);
        dangle=360/nangle;
        dz=sqrt(3)*d;
        zstart=.22;
        zn=6;
        echo("Antal rundt: ",nangle);
        echo("Vinkel rundt: ",dangle);
        echo("Afstand rundt: ",2*pi*R1/nangle);
        echo("Afstand op: ",dz);
        echo("Samlet højde",(zn+.5)*dz);
        for (z=[0:zn-1]) {
            for (a=[0:nangle-1]) {
                pin(zstart+z*dz, angle=a*dangle);
                if (z<zn-1) pin(zstart+(z+.5)*dz, angle=a*dangle+dangle/2);
            }
        }
    }

    module pin(z=0,angle=0) {
        d=.3;
        out=.09;
        rotate([0,0,angle])
            translate([R1-sqrt(3)/2*d+out,0,z]) 
                rotate([0,45,45]) cube([d,d,d],center=true);
    }

    if (parts[0]) {
        if (color) {
            color(color) {
                hane();
                fod();
            }
        } else {
            hane();
            fod();
        }
    }
    if (parts[2]) powerled();
}

quooker(parts=[1,1,1]);