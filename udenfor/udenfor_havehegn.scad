module post() {
    cylinder(195,7,7,$fn=30);
    translate([0,0,195]) cylinder(5,7,4,$fn=30);
}

for (x=[0:4]) {
    translate([200*x,0,0]) {
    post();
    for (z=[0:24])
        translate([-12,11.4,6+7.5*z])
            rotate([5.5,90,0]) 
                cylinder(225,3.5,3.5,$fn=20);
    }
}

translate([1000,0,0]) post();