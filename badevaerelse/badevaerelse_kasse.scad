use <MCAD/boxes.scad>;

W=43;
H=47;
D=29;
T=1.2;

space=5;

d=1.6;

module lid() {

    difference() {
        translate([0,0,-.65])
        roundedBox([W+1,D+1,T+1.3],2,sidesonly=true,$fn=20);
        cube([23*d, 11*d,1.1*D], center=true);
    }
    translate([0,0,-T/4]) {
        for (x=[-11:2:11]) {
            translate([x*d,0,0])
                cube([d,11*d+.1,T/2],center=true);
        }
    }
    translate([0,0,T/4]) {
        for (y=[-4:2:4]) {
            translate([0,y*d,0])
                cube([23*d+.1,d,T/2],center=true);
        }
    }
}

module box() {
    difference() {
        translate([0,0,-1.5]) {
            difference() {
                roundedBox([W-.5,D-.5,H-T],1.5,sidesonly=true,$fn=20);
                translate([0,0,(H-T)/2-19*d/2])
                    cube([W-.5-3,D-.5-3,H-T],center=true);
                translate([0,0,-(H-T)/2])
                    cube([W-13,1.1*D,6],center=true);
                translate([0,0,-(H-T)/2])
                    cube([1.1*W,D-13,6],center=true);
            }
        }
        cube([23*d,D+1,19*d],center=true);
        cube([W+1,11*d,19*d],center=true);
    }
}

module boxpins() {
    for (q=[-1,1]) {
        for (x=[-8:2:8]) {
            translate([0,q*((D-.5)/2-T/4),x*d])
                cube([23*d+.1,T/2,d],center=true);
        }
        for (x=[-11:2:11]) {
            translate([x*d,q*((D-.5)/2-3*T/4),0])
                cube([d,T/2,19*d+.1],center=true);
        }
        for (x=[-8:2:8]) {
            translate([q*((W-.5)/2-T/4),0,x*d])
                cube([T/2,11*d+.1,d],center=true);
        }
        for (x=[-4:2:4]) {
            translate([q*((W-.5)/2-3*T/4),x*d,0])
                cube([T/2,d,19*d+.1],center=true);
        }
    }
}
//    color("red") {
//        translate([0,0,T/4]) {
//            for (y=[-4:2:4]) {
//                translate([0,y*d,0])
//                    cube([23*d+.1,d,T/2],center=true);
//            }
//        }
//    }

box();
translate([0,0,H/2])
  lid();
boxpins();