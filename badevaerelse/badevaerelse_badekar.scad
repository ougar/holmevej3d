dif=.01;

D=58;
L=146;
Hout=49;
edge=4;
edgefront=5;

R1=16;
r1=1.5;
rout=2;

tub_r=7;
tub_r2=10;
tub_depth=40;
tub_depth_move=tub_depth-tub_r;
tub_depth_correct=7;
tub_side=5;
tub_foot=3;
tub_head=20;

drain_x=D/2;
drain_y=16;
drain_z=-tub_depth+.2;
drain_r1=3.5;
drain_r2=2;
drain_dz=1;

module cylcorn(corn) {
    rotate([0,0,90*corn]) {
        translate([-R1,-R1,0])
            rotate_extrude(angle=90, $fn=100)
        translate([R1,0,0])
            circle(r1);
        difference() {
            translate([-R1/2+r1/2,-R1/2+r1/2,0]) 
                cube([R1+r1,R1+r1,2*r1],center=true);
            translate([-R1,-R1,0])
                cylinder(10,R1,R1,$fn=40,center=true);
        }
    }
}

module cyledge() {
    translate([0,R1-dif/2,0]) rotate([-90,0,0]) cylinder(L-2*R1+dif,r1,r1,$fn=40);
    translate([D,R1-dif/2,0]) rotate([-90,0,0]) cylinder(L-2*R1+dif,r1,r1,$fn=40);
    translate([R1-dif/2,0,0]) rotate([0, 90,0]) cylinder(D-2*R1+dif,r1,r1,$fn=40);
    translate([R1-dif/2,L,0]) rotate([0, 90,0]) cylinder(D-2*R1+dif,r1,r1,$fn=40);
    cylcorn(2);
    translate([0,L,0]) cylcorn(1);
    translate([D,0,0]) cylcorn(3);
    translate([D,L,0]) cylcorn(0);
}

module tub() {
    hull() {
        translate([R1,  R1,  0]) sphere(R1-r1,$fn=50);
        translate([R1,  L-R1,0]) sphere(R1-r1,$fn=50);
        translate([D-R1,R1,  0]) sphere(R1-r1,$fn=50);
        translate([D-R1,L-R1,0]) sphere(R1-r1,$fn=50);
        
        translate([ tub_side, tub_foot, -tub_depth_move]) translate([tub_r,  tub_r,  0]) sphere(tub_r,$fn=50);
        translate([-tub_side, tub_foot, -tub_depth_move]) translate([D-tub_r,tub_r,  0]) sphere(tub_r,$fn=50);
        translate([-tub_side,-tub_head, -tub_depth_move+tub_depth_correct])
            translate([D-tub_r,L-R1,0]) 
                rotate([30,0,0]) scale([1,3,2]) sphere(tub_r,$fn=50);
        translate([ tub_side,-tub_head, -tub_depth_move+tub_depth_correct])
            translate([tub_r,L-R1,0]) 
                rotate([30,0,0]) scale([1,3,2]) sphere(tub_r,$fn=50);
    }
}
   
module box() {
    translate([-edge,-edge,-Hout]) cube([D+2*edge,L+2*edge,Hout]);
    translate([-edge,-edge,-1])   cube([edge,L+2*edge,r1+1]);
    translate([D,-edge,-1])       cube([edge-rout+1,L+2*edge,r1+1]);
    translate([-edge,-edge,-1])   cube([D+edge+r1,edge,r1+1]);
    translate([-edge,L,-1])       cube([D+edge+r1,edge,r1+1]);
    translate([D+edge-rout+1,-edge,r1-rout]) rotate([-90,0,0]) cylinder(L+2*edge,rout,rout,$fn=40);
}

module wall() {
    color("lightblue")
    translate([D+edge,-1.01*edge,-Hout])
    cube([1,L+2.02*edge,Hout]);
}

module drain() {
    deep=5;
    moved=1.3;
    holer=.6;
    color("gray") {
        translate([drain_x,drain_y,drain_z-deep]) {
            difference() {
                cylinder(deep,drain_r1,drain_r1,$fn=40);
                translate([0,0,deep-drain_dz+dif]) 
                    cylinder(drain_dz,0,drain_r1,$fn=40);
                translate([0,0,deep-2.5])
                    cylinder(2.5,drain_r2,drain_r2,$fn=40);
                translate([0,0,deep-5.5])
                    cylinder(2.5,drain_r2,drain_r2,$fn=40);
                translate([0,0,-1]) {
                    for (a=[0:5])
                        rotate([0,0,360/6*a])
                            translate([moved,0,0])
                                cylinder(deep,holer,holer,$fn=20);
                    cylinder(deep,holer,holer,$fn=20);
                }
            }
        }
    }
}

module drain_hole() {
    translate([drain_x,drain_y,drain_z-10])
        cylinder(20,drain_r1,drain_r1,$fn=40);
}

module badekar() {
    difference() {
        union() {
            box();
            cyledge();
        }
        tub();
        drain_hole();
    }
}

badekar();
drain();
wall();