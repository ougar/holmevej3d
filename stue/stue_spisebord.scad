use <MCAD/boxes.scad>;

height=73;
depth=90;
width=150;
edgerad=.5;

thick=3;

ins=.5;
ins2=2;
leg=4;
lspace=1.6;
edgeh=6.7;
edged=2;
edgesink=.5;
edgesinkh=1;

module leg() {
    translate([leg/2,leg/2,0])
    roundedBox([leg,leg,height-thick],edgerad,sidesonly=true,$fn=20);
    translate([leg/2+lspace+leg,leg/2,0])
    roundedBox([leg,leg,height-thick],edgerad,sidesonly=true,$fn=20);
    translate([leg/2,leg/2+lspace+leg,0])
    roundedBox([leg,leg,height-thick],edgerad,sidesonly=true,$fn=20);
    translate([leg+lspace/2,leg/2+.4,0])
        cube([leg,leg-.8,height-thick],center=true);
    translate([leg/2+.4,leg+lspace/2,0])
        cube([leg-.8,leg,height-thick],center=true);
}

module legs() {
    rot=[270,0,180,90];
    dx=[-1,-1,1,1]*(width/2-ins);
    dy=[1,-1,1,-1]*(depth/2-ins);
    for (q=[0,1,2,3])
        translate([dx[q],dy[q],height/2-thick/2])
            rotate([0,0,rot[q]])
                leg();
}

module stivere() {
    for (q=[-1:2:1]) {
        translate([0,q*(depth/2-ins2),height-thick-edgeh/2-1]) {
            roundedBox([width-8*ins2,edged,edgeh],edgerad,$fn=20);
            translate([0,0,edgesinkh])
                cube([width-5*ins2,edged-edgesinkh,edgeh],center=true);
        }
        translate([q*(width/2-ins2),0,height-thick-edgeh/2-1]) {
            roundedBox([edged,depth-8*ins2,edgeh],edgerad,$fn=20);
            translate([0,0,edgesinkh])
                cube([edged-edgesinkh,depth-5*ins2,edgeh],center=true);
        }
    }
}

module bordplade() {
    translate([0,0,height-thick/2-.01])
        difference() {
            roundedBox([width,depth,thick],edgerad,$fn=40);
                cube([.2,depth+1,thick+.01],center=true);
        }
}

legs();
stivere();
bordplade();
