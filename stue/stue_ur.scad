D=5;
T=.3;
R=11.5;
scale1=.95;
scale2=1.15;

edge=1.2;

dc=R*scale1-T;
glassd=.3;

fn=200;

module case() {
    difference() {
        linear_extrude(D,scale=scale1)
            circle(R,$fn=fn);
        translate([0,0,-.01]) {
            linear_extrude(D+.02, scale=scale2)
                circle(R*scale1/scale2-T,$fn=fn);
            cylinder(D-edge,dc,dc,$fn=fn);
        }
    }
}

module glass() {
    translate([0,0,D-edge-glassd]) 
        cylinder(glassd,dc,dc,$fn=fn);
}

dot1R=.2;
dot2R=.07;

cyld1=dc+.4;
cyld2=dc+-.1;
cylh=2;

faceh=D-edge-glassd-cylh-.1+.3;

module face() {
    translate([0,0,D-edge-glassd-cylh-.1]) {
        difference() {
            cylinder(cylh,cyld1,cyld1,$fn=fn);
            translate([0,0,.3]) cylinder(cylh,cyld2,cyld2,$fn=fn);
        }
    }
}

module viser(h,l,d=.25,pos=0) {
    translate([0,0,h]) {
        cylinder(.1,.6,.6,$fn=20);
        rotate([0,0,-6*pos]) {
            hull() {
                translate([0,l[0],0])  cylinder(.1,.1,.1,$fn=3);
                translate([0,0,0])  cylinder(.1,d,d,$fn=3);
                translate([0,l[1],0]) cylinder(.1,.1,.1,$fn=3);
            }
        }
    }
}
    

module clock() {
    translate([0,0,faceh]) {
        translate([0,5,0]) {
            linear_extrude(.1) {
                text("GEORG JENSEN", size=.7,halign="center",valign="center",$fn=20);
                translate([0,-1,0]) 
                    text("DENMARK", size=.4,halign="center",valign="center",$fn=20);
            }
        }
        for (x=[1:60])
            rotate([0,0,-x*6])
                translate([0,cyld2*.88])
                    if (x%5==0)
                        cylinder(.1,dot1R,dot1R,$fn=10);
                    else
                        cylinder(.1,dot2R,dot2R,$fn=10);
        cylinder(1,.2,.2,$fn=20);
        // Time
        viser(.2,[7,-2],.25,6.25);
        // Minut
        viser(.5,[9,-2],.25,14.5);
        // Sekund
        viser(.8,[10,-3],.15,33);
    }
}

color("lightgray") case();
color("white") face();
color("#333333") clock();
color("#AAAACC", alpha=.2) glass();
