use <MCAD/boxes.scad>;

H1=129;
H2=64;
D=40;
W=120;

d=2;

feet=4;
draw1=22;
draw2=10;
tv_z=74;
tv_y=20;
hylde=14;
extra=3;

module buttom_box() {
    difference() {
        roundedBox([W,D,H1],1, $fn=20);
        translate([0,D/2-tv_y/2+.1,-H1/2+tv_z/2+feet+draw1+draw2])
            cube([W-2*d,tv_y,tv_z], center=true);
        translate([0,2,-H1/2+(draw1+draw2-d)/2+feet+.25*d]) {
            translate([-W/4,0,0]) cube([W/2-2*d,D,draw1+draw2-1.5*d], center=true);
            translate([W/4,0,0]) cube([W/2-2*d,D,draw1+draw2-1.5*d], center=true);
        }
        translate([0,2,-H1/2+tv_z+feet+draw1+draw2+d+hylde/2]) {
            translate([-W/4,0,0]) cube([W/2-2*d,D,hylde], center=true);
            translate([W/4,0,0]) cube([W/2-2*d,D,hylde], center=true);
        }
    }
}

module drawers() {
    translate([0,D/2+d/2,-H1/2+draw1/2+feet]) {
        translate([-W/4,0,0]) roundedBox([W/2-.3,d,draw1],$fn=20,.5);
        translate([W/4,0,0]) roundedBox([W/2-.3,d,draw1],$fn=20,.5);
    }
}

module top_box() {
    translate([0,1,(H1+H2)/2]) {
        difference() {
            roundedBox([W,D+2,H2],1,$fn=20);
            translate([0,2,0]) {
                translate([-W/4,0,0]) cube([W/2-2*d,D,H2-2*d], center=true);
                translate([W/4,0,0]) cube([W/2-2*d,D,H2-2*d], center=true);
            }
        }
            translate([0,D/2+d/2,0]) doors();
    }
}

module doors() {
    translate([-W/4,0,0]) roundedBox([W/2-.3,d,H2-.3],$fn=20,.5);
    translate([W/4,0,0])  roundedBox([W/2-.3,d,H2-.3],$fn=20,.5);
}


buttom_box();
drawers();
top_box();
/*

dh=.2;
dw=.1;
but=2;
h1=14.5;
h2=H-4*(h1+dh)-but-dh;
lidd=2;

cube([W,D-lidd,H],center=true);

module drawer(x) {
    translate([0,0,x*(h1+dh)])
        roundedBox([W-dw,lidd,h1],.5,$fn=20);
}

translate([0,D/2,-H/2+h1/2+but]) {
    drawer(0);
    drawer(1);
    drawer(2);
    drawer(3);
}
translate([0,D/2,-H/2+h2/2+4*(h1+dh)+but])
    roundedBox([W-dw,lidd,h2],.5,$fn=20);
    
*/