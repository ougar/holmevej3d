use <MCAD/boxes.scad>;

height=52;
depth=75;
width=110;
plate=34;
edgerad=.5;

thick=3;
leg=5;
edgeh=5;
edged=2;
ins=3.2;
ins2=2.5;

module legs() {
    for (x=[-1:2:1])
        for (y=[-1:2:1])
            translate([x*(width/2-ins),y*(depth/2-ins),height/2-thick/2])
                roundedBox([leg,leg,height-thick],edgerad,$fn=20,sidesonly=true);
}

module stivere() {
    for (q=[-1:2:1]) {
        translate([0,q*(depth/2-ins2),height-thick-edgeh/2])
            cube([width-2*ins,edged,edgeh],center=true);
        translate([q*(width/2-ins2),0,height-thick-edgeh/2])
            cube([edged,depth-2*ins,edgeh],center=true);
    }
}

module bordplade() {
    translate([thick/2,0,height-thick/2-.1])
        roundedBox([width+thick,depth,thick],edgerad,$fn=40);
}

module extraplade() {
    translate([width/2+thick/2,0,height-plate/2-thick-1])
        roundedBox([thick,depth,plate],edgerad,$fn=40);
}

legs();
stivere();
bordplade();
extraplade();