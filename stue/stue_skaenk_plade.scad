D=43.5;
T=4;
W=218;
r1=.2;
r2=.7;

module plate1(w) {
    translate([-T/2+r1,r1,0]) cylinder(w,r1,r1,center=true,$fn=40);
    translate([ T/2-r2,r2,0]) cylinder(w,r2,r2,center=true,$fn=40);
    translate([-T/2+r1,0,-w/2]) cube([T-r1-r2,3,w]);
    translate([-T/2,r1,-w/2]) cube([2*r1,3-r1,w]);
    translate([T/2-2*r2,r2,-w/2])
      cube([2*r2,3-r2,w]);
    translate([0,D/2+1.5,0]) cube([T,D-3,w],center=true);
}


module plade() {
    translate([0,0,-W/2-2]) plate1(W);
    difference() {
        intersection() {
            plate1(10);
            translate([0,D/2,0]) rotate([-90,0,0]) plate1(D);
        }
        cube([10,2*r2,2*r2],center=true);
    }
    intersection() {
        plate1(10);
        translate([0,D/2,0]) rotate([-90,0,0]) plate1(D);
        translate([0,r2,-r2]) rotate([0,90,0]) cylinder(10,r2,r2,$fn=40,center=true);
    }
}

rotate([0,-90,0]) plade();
