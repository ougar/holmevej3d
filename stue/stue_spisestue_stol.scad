R1=300;
R2=100;
r=3.4/2;
H=98;
D=40;
pi=3.141592654;
angle=(H/R1*180/pi);
angle2=(D+2*r+.26)/R2*180/pi;

h=R2-R2*cos(angle2/2)-.3285;

dx=38;
dz=38.5;
legh=44;

module langben() {
    for (y=[-.5:.5])
    translate([0,-r-.072,y*D]) {
    
        rotate([-y*6,0,0])
        
        translate([-R1,0,0])
        rotate_extrude(angle=angle, $fn=200) {
            translate([R1,0,0])
                circle(r);
        }
    }
}

module cylcut(s=1, t=0) {
    translate([0,4,s*(D/2+3.05)])
    rotate([-s*47,0,0])
    rotate([0,s*(angle2/2-4),0]) 
    translate([0,0,t]) cube([10,10,10],center=true);
}

module top() {
    translate([-h,0,0])
    rotate([90,0,180])
    translate([-R2,0])
    rotate([0,0,-angle2/2])
    rotate_extrude(angle=angle2, $fn=200) {
        translate([R2,0,0])
            circle(r);
    }
}
module backframe() {
    difference() {
        langben();
        cylcut(-1,10);
        cylcut(1,-10);
    }
    difference() {
        top();
        cylcut(-1);
        cylcut(1);
    }
}

module front_legs() {
    translate([dx,38,0]) {
        translate([0,0,-dz/2]) rotate([90,0,0]) cylinder(legh,r,r,$fn=40);
        translate([0,0, dz/2]) rotate([90,0,0]) cylinder(legh,r,r,$fn=40);
        
        translate([0,-30,0]) cylinder(38,1,1,center=true,$fn=40);
    }

    translate([dx/2,11,-dz/2+1.4]) 
    rotate([0,94.5,0])
    cylinder(38,1,1,center=true,$fn=40);

    translate([dx/2,11,+dz/2-1.4]) 
    rotate([0,-94.5,0])
    cylinder(38,1,1,center=true,$fn=40);
}

module seatblocks() {
    // Front
    translate([dx,-4,0]) cube([2,4,dz], center=true);
    // Sides
    translate([19,-4,-18.1]) rotate([0, 3.1,0]) cube([dx,4,2],center=true);
    translate([19,-4, 18.1]) rotate([0,-3.1,0]) cube([dx,4,2],center=true);
    // Back
    translate([0,-4.5,0]) cube([2,5,35],center=true);
}

stiv=50;
module stiver() {
    translate([0,0,stiv/2])
    difference() {
        union() {
            translate([0,.5,0]) cylinder(stiv,.7,.7,$fn=40, center=true);
            translate([0,-.5,0]) cylinder(stiv,.7,.7,$fn=40, center=true);
            cube([1,1,stiv], center=true);
        }
        translate([2,0,0]) cube([3,3,stiv+1],center=true);
        translate([-2,0,0]) cube([3,3,stiv+1],center=true);        
    }
}

module stivere() {
    translate([0,0,6.5]) rotate([0,-8.4,0]) stiver();
    translate([0, 4.1  ,6.5]) rotate([-2.5,-8.1,  -1]) stiver();
    translate([0,-4.1  ,6.5]) rotate([ 2.5,-8.1,   1]) stiver();
    translate([0, 2*4.1,6.5]) rotate([-5,  -7.7,-2.5]) stiver();
    translate([0,-2*4.1,6.5]) rotate([ 5,  -7.7, 2.5]) stiver();
}

sd=3;
sz=7;
cutd=6;
seatd = 36;
seatw1 = 41;
seatw2 = 34;

module seat() {
    color("#555555")
    hull() 
    difference() {
        union () {
            translate([sd,-seatw2/2,sz]) sphere(2,$fn=30);
            translate([sd, seatw2/2,sz]) sphere(2,$fn=30);
            translate([sd+seatd,-seatw1/2,sz]) sphere(2,$fn=30);
            translate([sd+seatd, seatw1/2,sz]) sphere(2,$fn=30);
        }
        translate([20,0,cutd-5]) cube([50,50,10],center=true);
    }
}


rotate([-90,0,0]) {
    difference() {
        union() {
            translate([-R1,0,0])
            rotate([0,0,-11])
            translate([R1,0,0]) 
                backframe();
            front_legs();
        }
        // Flat bottom
        translate([0,43,0]) cube([100,10,100],center=true);
    }
    seatblocks();
}
stivere();
seat();