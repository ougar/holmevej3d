width=150;
height=80;
depth=8;
thick=1;
elevation=10;

module radiator() {
    translate([0,0,elevation]) {
        union() {
            flat();
            ripples();
        }
    }
}

module flat() {
    cube([width+.4,thick,height]);
    translate([0,depth,0])
        cube([width+.4,thick,height]);
    translate([1,-.9,1])
        cube([width-2+.4,thick,height-2]);
}

module ripples() {
    for (x=[0:50]) {
        translate([3*x,thick/2,0])
            cube([.4,depth,height]);
    }
}

piperad=1.2;
bend=5;
h1=80;
h2=15;

module pipe(vdist) {  
    difference() {
        rotate_extrude()
            translate([bend, 0, 0])
                circle(r=piperad, $fn=20);
        translate([0,-15,-10]) cube([20,30,20]);
        translate([-15,-20,-10]) cube([30,20,20]);
    }
    //rotate([0,90,0])
    //    translate([0,bend,0])
    //        cylinder(hdist,piperad,piperad, $fn=20);
    rotate([90,0,0])
        translate([-bend,0,0])
            cylinder(vdist,piperad,piperad, $fn=20);
}


module leftpipe() {
    translate([.5,depth/2,h1])
        rotate([90,0,0])
            pipe(h1);
    translate([.5-bend,-5,70])
        rotate([270,0,0]) {
            cylinder(7,3,3,$fn=20);
            cylinder(9,1.2,1.2,$fn=20);
        }
}

module rightpipe() {
    translate([width-.1,depth/2,h2])
        rotate([90,0,180])
            pipe(h2);

    translate([width-.1,depth/2,height+elevation-3]) {
        rotate([0,90,0]) {
            cylinder(2,.7,.7,$fn=20);
            translate([0,0,2])
                cylinder(.5,1.4,1.4,$fn=20);
        }
    }
}

union() {
    radiator();
    leftpipe();
    rightpipe();
}