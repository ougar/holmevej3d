use <MCAD/boxes.scad>;

W=13;
D=10;
R1=.6;
din=.2;
H=.7;
space=.06;
dedge=1;
dmidt=1;
D2=D-2*dedge;
W2=(W-2*dedge-dmidt)/2;
R2=.2;
t=.5;

module frame() {
    difference() {
        hull() {
    //    union() {
            roundedBox([W,D,.1], R1, sidesonly=true, $fn=40);
            translate([0,0,H]) roundedBox([W-din,D-din,.1], R1, sidesonly=true, $fn=40);
        }
        translate([-W2/2-dmidt/2,0,0]) roundedBox([W2,D2,4],R2,$fn=20,sidesonly=true);
        translate([ W2/2+dmidt/2,0,0]) roundedBox([W2,D2,4],R2,$fn=20,sidesonly=true);
    }
}

module switch(on=1) {
    translate([0,D2/4,.2]) {
        rotate([on*5,0,0]) {
            roundedBox([W2-2*space,D2/2-1.5*space,.5], R2, $fn=20, sidesonly=true);
        }
    }
}

module switches() {
    translate([-(W2+dmidt)/2,0,H-t/2+.1]) switch();
    translate([ (W2+dmidt)/2,0,H-t/2+.1]) switch(on=-1);
}

module plug() {
    difference() {
        roundedBox([W2-2*space,D2-1.5*space,.5], R2, $fn=20, sidesonly=true);
        translate([-1,-D2/4,0]) color("black") cylinder(1,.25,.25,$fn=20,center=true);
        translate([ 1,-D2/4,0]) color("black") cylinder(1,.25,.25,$fn=20,center=true);
        translate([ 0,-1.7*D2/4,0]) {
            difference() {
                color("black") cylinder(1,.25,.25,$fn=20,center=true);
                translate([0,1.6,0]) cube([3,3,3],center=true);
            }
        }
    }
}

module plugs() {
    difference() {
        union() {
            translate([-W2/2-dmidt/2,0,H-t/2+.1])
                plug();
            translate([ W2/2+dmidt/2,0,H-t/2+.1])
                plug();
        }            
        translate([0,D/2-space/2,0]) cube([W,D,4],center=true);
    }
}

module dobbelt_afbryder() {
    frame();
    plugs();
    switches();
}

dobbelt_afbryder();