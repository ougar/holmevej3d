//linear_extrude(height = 10, center = true, convexity = 10, twist = 0)
//    translate([2, 0, 0])
//        circle(r = 1,$fn=20);
        
module torus2(r1, r2) {
    rotate_extrude(angle=90,convexity=10, $fn=100) translate([r1,0,0]) circle(r2);
}

module oval_torus(inner_radius, thickness=[0, 0]) {
    rotate_extrude() translate([inner_radius+thickness[0]/2,0,0]) ellipse(width=thickness[0], height=thickness[1]);
}

rotate([0,0,45])
difference() {
torus2(25,1);
translate([10,10,0])
rotate([0,0,-45])
cube([50,10,3],center=true);
}