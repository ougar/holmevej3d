module digit(n, height=1, size=1, dist=.1, width=.2, angle=0) {
    for (x=segseq(n)) {
        translate(size*segpos(x))
            rotate(segrot(x))
                segment(height,size,dist,width,angle);
    }
}

module segment(height=1,size=1,dist=.1,width=.2,angle=0) {
    h=height;
    w=size*width;
    d=size*dist;
    s=w/sqrt(2);
    l=size-w-d;
    
    translate([0,w/2+d/2,h/2]) {
        rotate([0,0,45])
            cube([s,s,h],center=true);
        translate([0,l,0])
            rotate([0,0,45])
                cube([s,s,h],center=true);
        translate([0,l/2,0])
            cube([w,l,h],center=true);
    }
}

function segpos(n) = 
  n == 1 ? [0,2,0] :
  n == 2 ? [0,1,0] :
  n == 3 ? [1,1,0] :
  n == 4 ? [0,1,0] :
  n == 5 ? [0,0,0] :
  n == 6 ? [1,0,0] :
  n == 7 ? [0,0,0] : 
  [];

function segrot(n) = 
  n == 1 ? [0,0,-90] :
  n == 2 ? [0,0,0]  :
  n == 3 ? [0,0,0]  :
  n == 4 ? [0,0,-90] :
  n == 5 ? [0,0,0]  :
  n == 6 ? [0,0,0]  :
  n == 7 ? [0,0,-90] : 
  [];

function segseq(n) =
  n == 0 ? [1,2,3,5,6,7] :
  n == 1 ? [3,6] :
  n == 2 ? [1,3,4,5,7] :
  n == 3 ? [1,3,4,6,7] :
  n == 4 ? [2,3,4,6] :
  n == 5 ? [1,2,4,6,7] :
  n == 6 ? [1,2,4,5,6,7] :
  n == 7 ? [1,3,6] :  
  n == 8 ? [1,2,3,4,5,6,7] :
  n == 9 ? [1,2,3,4,6,7] :
  [];

digit(7, size=2);