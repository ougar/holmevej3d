use <MCAD/boxes.scad>;
use <skab_haandtag.scad>;

module hskab(W,D,H,T=2,N=[1],
             r=.3,dopen=.3, dt=.2,foot=[0,0],
             parts=[true,true,true], hplace=false,hx=3,hdx=3,hdz=11) {
    skab=parts[0];
    door=parts[1];
    hand=parts[2];
                         
    skab(W=W,D=D,H=H,T=T,N=N,r=r,dopen=dopen, dt=dt,foot=foot,parts=parts);
                         
    dy = D/2+T+dt;
    dx = W/2-hdx;
    dz = H/2-hdz;
                         
    function getTranslation(place) = 
        place == "ul" ? [ dx, dy,  dz] :
        place == "ur" ? [-dx, dy,  dz] :
        place == "lr" ? [-dx, dy, -dz+foot[1]] :
        place == "ll" ? [ dx, dy, -dz+foot[1]] :
        [0, 0, 0];
                         
    if (hand && hplace) {
        if (hplace=="drawer") {
            for (x=[0:len(N)-1]) {
                translate([0,dy,-H/2+foot[1]+(N[x]+height(N,x))*(H-foot[1])-hx]) rotate([0,90,0]) handle();
            }
        } else {
            translate(getTranslation(hplace)) handle();
        }
    }
}

// skab_haandtag(40,60,70,hand="drawer");   
       
module skab(W,D,H,T=2,N=[1],r=.3,dopen=.3, dt=.2,foot=[0,0],parts=[true,true]) {
    skab=parts[0];
    door=parts[1];
    H2=H-foot[1];
    if (skab) {
        difference() {
            cube([W,D,H], center=true);
            translate([0,D/2-foot[0]/2+.1,-H/2+foot[1]/2-.1]) 
                cube([W+1,foot[0]+.1,foot[1]+.1],center=true);
        }
    }
    if (door) {
        for (x=[0:len(N)-1]) {
            echo("Making",x,height(N,x),-H/2+foot[1]+height(N,x)*H+N[x]/2);
            translate([0,D/2+T/2+dopen,-H/2+foot[1]+H2*N[x]/2+H2*height(N,x)]) {
                roundedBox([W-2*dt,T,N[x]*(H-foot[1])-2*dt],r,$fn=30);
            }
        }
    }
}

function height(x,n)=((n==0)?0:x[n-1]+height(x,n-1));

module skraa(W,D,H,T=2,r=.3,dopen=.3, dt=.2, parts=[true,true,true],hplace=false,hdx=3,hdz=11) {
    if (parts[0])
        rotate([0,-90,0])
            linear_extrude(W, scale=[H[1]/H[0],1])
                square([H[0],D]);
    if (parts[1])
        translate([-W+dt,D+T+dopen,dt])
            skraa_laage(W-2*dt,[H[0]-2*dt+(dt/W)*(H[1]-H[0]),H[1]-2*dt-(dt/W)*(H[1]-H[0])],
                        T,r,dopen,dt);
    function getdx(hplace) = hplace=="r" ? hdx-W/2 : hplace=="l" ? W/2-hdx : 0;
    if (parts[2])
        translate([-W/2+getdx(hplace),D+T+dopen,hdz])
            handle();
}


module skraa_laage(W,H,T,r,dopen,dt) {
    translate([r,   -r, r     ]) sphere(r,$fn=30);
    translate([r,   -r, H[1]-r]) sphere(r,$fn=30);
    translate([W-r, -r, r     ]) sphere(r,$fn=30);
    translate([W-r, -r, H[0]-r]) sphere(r,$fn=30);
    translate([r,0,r]) {
        rotate([0,-90,180])
            linear_extrude(W-2*r, scale=[(H[0]-2*r)/(H[1]-2*r),1])
                square([H[1]-2*r,T]);
    }
    translate([r,-r,r])
        cylinder(H[1]-2*r,r,r,$fn=30);
    translate([W-r,-r,r])
        cylinder(H[0]-2*r,r,r,$fn=30);    // 
    translate([r,-r,r])
        rotate([0,90,0])
            cylinder(W-2*r,r,r,$fn=30);

    L=sqrt((H[0]-H[1])^2+(W-2*r)^2);
    translate([r,-r,H[1]-r])
        rotate([0,90-atan((H[0]-H[1])/(W-2*r)),0])
            union() {
                translate([-r,-T+r,0])
                    cube([2*r,T-r,L]);
                cylinder(L,r,r,$fn=30);
            }
       
    translate([r,-r,r])
        rotate([90,0,0])
            cylinder(T-r,r,r,$fn=30);
    translate([W-r,-r,r])
        rotate([90,0,0])
            cylinder(T-r,r,r,$fn=30);
    translate([r,-r,H[1]-r])
        rotate([90,0,0])
            cylinder(T-r,r,r,$fn=30);
    translate([W-r,-r,H[0]-r])
        rotate([90,0,0])
            cylinder(T-r,r,r,$fn=30);
    translate([r,-T,0])
        cube([W-2*r,T-r,r]);
    translate([0,-T,r])
        cube([r,T-r,H[1]-2*r]);
    translate([W-r,-T,r])
        cube([r,T-r,H[0]-2*r]);
}

translate([-20,20,-30])
skab(40,40,60,T=2);

skraa(40,40,[60,80],hplace="r",T=2);



// Kontor syskabe
//skab(40,36,69,footup=0,footin=0,N=[1]);

// Kontor skrivebordsskabe
//skab(40,36,60,footup=0,footin=0,N=[1]);

// Kontor elektronikskabe
//skab(60,20,64,footin=0,footup=0,dt=.2,dopen=.2);
//translate([60.4,0,0])skab(60,20,64,footin=0,footup=0,dt=.2,dopen=.2);

// Skænk
module skaenk(nparts=[1,1,1]) {
    dd=.3; W=40; D=40; foot=[11,7]; H=80+foot[1]; 
    for (x=[0:3])
        translate([x*(W+dd),0,0]) hskab(W,D,H,foot=foot,parts=nparts);
    translate([4*(dd+W),0,0])
        difference() {
            hskab(W,D,H,N=[.75,.25],foot=foot,parts=nparts,hplace="drawer");
            translate([W/2,0,-H/2]) cube([2,42,2*foot[1]],center=true);
        }   
    translate([4.5*(dd+W)+.8,1,foot[1]/2])
        roundedBox([1.5,40+2,80],.3,$fn=20);
}

// skaenk([1,1,1]);