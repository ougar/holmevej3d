module handle() {
    r=.45;

    R=9.4;
    S=3;
    D=10.5;
    pi=3.1415926;

    angle=D/(2*pi*R)*360;

    calch=2*sin(angle/2)*(R+r)+2*S*cos(angle/2);
    calcd=(R+r)*cos(angle/2)-S*sin(angle/2);

    // echo("Højde",calch);
    // echo("Hul  ",R-r-calcd);

    module bighandle() {
        rotate_extrude(angle=angle, $fn=100)
            translate([R,0,0])
                circle(r);

        translate([R,-S/2,0]) rotate([90,0,0])
            translate([0,0,-.1]) cylinder(S+.02,r,r,$fn=100, center=true);    

        translate([cos(angle), sin(angle), 0]*R)
        rotate([-90,0,angle])
        translate([0,0,-.01]) cylinder(S+.02,r,r,$fn=100);    
    }
    rotate([90,0,90])
    translate([-1-calcd+.95,0,0])
        difference() {
            rotate([0,0,-angle/2]) color("gray") bighandle();
            translate([calcd-.95,0,0])
                cube([2,calch,2],center=true);  // Cut ends
    }
}

handle();