D=46;
W=80;

T=3;
topd=.4;
drawd=.4;
drawout=.5;

foot=6.5;
top=T;
dip=1;
drawspace=2.5;
drawer=20.2;
H=5*(drawer+drawspace)+foot;
but=-H/2+foot+drawer/2;

module box() {
    translate([0,T/2,H/2+T/2+.1]) cube([W+topd,D+topd+T,T],center=true);
    difference() {
        cube([W,D,H],center=true);
        translate([0,D/2,-H/2]) cube([W-2*T,6,2*foot],center=true);
    }
}

module drawer(d,dh) {
    translate([0,D/2+T/2+drawout,dh]) {
        difference() {
            cube([d-drawd,T,drawer],center=true);
            translate([0,T/2,drawer/2+T/2]) 
                rotate([25,0,0]) 
                    cube([d,4*T,T],center=true);
        }
    }
}

color("white") {
box();
drawer(W,  but+0*(drawer+drawspace));
drawer(W,  but+1*(drawer+drawspace));
drawer(W,  but+2*(drawer+drawspace));
drawer(W,  but+3*(drawer+drawspace));
translate([-W/4,0,0]) drawer(W/2,but+4*(drawer+drawspace));
translate([ W/4,0,0]) drawer(W/2,but+4*(drawer+drawspace));
}