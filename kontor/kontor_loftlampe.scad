dist=38/2;
curve=39;
lamp_h=15;
lamp_d=12;
lamp_cut=4;
loft=100;

module lampe() {
    difference() {
        cylinder(lamp_h,lamp_d,0,$fn=40);
        translate([0,0,-1]) cylinder(lamp_h,lamp_d,0,$fn=40);
        translate([0,0,lamp_h]) cube([10,10,2*lamp_cut],center=true);
    }
    translate([0,0,4]) sphere(4,$fn=40);
}

module lamper() {
    translate([-dist,0,0]) lampe();
    translate([ dist,0,0]) lampe();
}

module spids() {
    r=lamp_d*lamp_cut/lamp_h;
    cylinder(lamp_cut,r+.3,.3,$fn=40);
    translate([0,0,lamp_cut-1]) cylinder(3,.8,.8,$fn=20);
}

module stang() {
    pi=3.1415;
    R=120;
    l=curve;
    angle=180*l/R/pi;
    r=.6;
    translate([0,0,lamp_h+1.5])
    rotate([0,-90,90])
    translate([-R,0,0])
        rotate([0,0,-angle/2])
            rotate_extrude(angle=angle,$fn=200)
                translate([R,0,0])
                    circle(r);
}

module base() {
    rad=12;
    intersection() {
        cylinder(4,6,6,$fn=100);
        translate([0,0,rad]) sphere(rad,$fn=100);
    }
}

module stativ() {
    translate([-dist,0,lamp_h-lamp_cut]) spids();
    translate([ dist,0,lamp_h-lamp_cut]) spids();
    stang();
    translate([0,0,loft]) base();
}

module ledning() {
    l=loft-lamp_h+2;
    d=.4;
    for (x=[-1,1])
        translate([x*dist,0,lamp_h+2])
            rotate([0,-x*11,0])
                cylinder(l,d,d,$fn=14);
}

color("white",alpha=.8) lamper();
%color("lightgray") stativ();
%color("black") ledning();
