height=74;
depth=80;
width=150;

corn=3;
plate=3;
edged=1.3;

legr1=2.5;
legr2=2;
legh2=10;
feeth=.5;

supportd=2.5;
supportr=.75;

module feet() {
    for (x=[-1:2:1])
        for (y=[-1:2:1])
            translate([x*(width/2-corn),y*(depth/2-corn),0])
                color("black")
                    cylinder(.5,1.1*legr2,1.1*legr2,$fn=30);
}

module support(l) {
    translate([-supportd/2,0,0])
    cylinder(l-2*corn,supportr,supportr,center=true,$fn=20);
    translate([supportd/2,0,0])
    cylinder(l-2*corn,supportr,supportr,center=true,$fn=20);
    cube([supportd,2*supportr,l-2*corn],center=true);
}

module ramme() {
    for (x=[-1:2:1])
        for (y=[-1:2:1])
            translate([x*(width/2-corn),y*(depth/2-corn),0]) {
                translate([0,0,feeth]) 
                    cylinder(legh2,legr2,legr2,$fn=30);
                translate([0,0,legh2]) 
                    cylinder(height-plate-legh2,legr1,legr1,$fn=30);
            }
    translate([0,depth/2-corn,height-plate-supportr-supportd/2])
        rotate([0,90,0])
            support(width);
    translate([0,-depth/2+corn,height-plate-supportr-supportd/2])
        rotate([0,90,0])
            support(width);
    translate([-width/2+corn,0,height-plate-supportr-supportd/2])
        rotate([90,0,0])
            rotate([0,0,90])
                support(depth);
    translate([width/2-corn,0,height-plate-supportr-supportd/2])
        rotate([90,0,0])
            rotate([0,0,90])
                support(depth);
}

module shelf() {
    H=50;
    W=20;
    D=60;
    d=2;
    h=5;
    y1=20;
    y2=D;
    translate([d/2+width/2-corn,-depth/2+corn,height-H-plate]) {
        translate([-W,0,0]) cube([W,D,d]);
        translate([-W-d,y1,-h]) {
            cube([d,d,H+h]);
            cube([W+d,d,h]);
        }
        translate([-W-d,y2-d,-h]) {
            cube([d,d,H+h]);
            cube([W+d,d,h]);
        }
        translate([-d,0,-h]) cube([d,depth-2*corn,h]);
        translate([-W-2*d,1.1*supportr,H-h]) cube([d,depth-2*corn-1.1*2*supportr,h]);
    }
}

module bordplade() {
    translate([0,0,height-plate/2-.1])
        color("gray") 
            cube([width-2*edged,depth-2*edged,plate],center=true);
}

module edge() {
    translate([0,depth/2-1.01*edged/2,height-plate/2])
        cube([width,edged,plate],center=true);
    translate([width/2-1.01*edged/2,0,height-plate/2])
        cube([edged,depth-1.9*edged,plate],center=true);
    translate([-width/2+1.01*edged/2,0,height-plate/2])
        cube([edged,depth-1.9*edged,plate],center=true);
    translate([-width/2,-depth/2+1.01*edged,height-plate/2])
        rotate([90,0,90]) difference() {
            cylinder(width,plate/2,plate/2,$fn=20);
            translate([0,-plate,-.05*width]) cube([2*plate,2*plate,1.1*width]);
        }
}

feet();
color("#eee") {
    ramme();
    shelf();
}
bordplade();
edge();