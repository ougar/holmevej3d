use <MCAD/boxes.scad>;

draw_w=28;
draw_h=9.2;
draw_dh=.4;
draw_dw=.2;
draw_d=2;
draw_edge=.3;
draw_r=7.5;
draw_cuth=2.4;

circle=1.4;
angle=130;


wdif=0;
hdif=1.5;
d=2;
H=d+6*(draw_h+draw_dh)+hdif;
W=draw_w+d+2*draw_dw;
D=38;
scut_r=1.4;
scut_h=.5;
scut_t1=.8;
scut_t2=1.2;



module skab() {
    for (x=[-.5,.5]) {
        translate([-x*W,0,0]) {
            cube([d,D,H],center=true);
            translate([0,D/2-scut_r+scut_h])
                intersection() {
                    cube([d,D,H],center=true);
                    cylinder(H,scut_r,scut_r,$fn=40,center=true);
                }
        }
    }
    topw=W+d+2*wdif;
    translate([0,0,H/2]) {
        //cube([topw,1.1*D,d],center=true);
        
        intersection() {
            // Plate with round sides
            union() {
                cube([topw,1.1*D,d],center=true);
                intersection() {
                    cube([topw*2,1.1*D,d],center=true);
                    for (x=[-.5,.5]) 
                        translate([x*(topw-2*scut_r+2*scut_t1),0,0])
                            rotate([90,0,0])
                                cylinder(1.1*D,scut_r,scut_r,center=true,$fn=40);
                }
            }
            // Plate with round front
            union() {
                cube([topw*1.1,D,d],center=true);
                intersection() {
                    cube([topw*1.1,2*D,d],center=true);
                    translate([0,D/2-scut_r+scut_t2,0])
                        rotate([0,90,0])
                            cylinder(1.1*topw,scut_r,scut_r,center=true,$fn=40);
                }
            }
        }
    }
}

module skuffe(H=0) {
    translate([0,(D-d)/2,H]) {
        intersection() {
            roundedBox([draw_w,draw_d,draw_h],draw_edge,$fn=20);
            translate([0,0,draw_r+1+draw_h/2-draw_cuth-1])
                rotate([90,angle/2+90,0])
                    rotate_extrude(angle=angle, $fn=100)
                        translate([draw_r+1,0,0])
                            circle(circle);
        }
        difference() {
            roundedBox([draw_w,draw_d,draw_h],draw_edge,$fn=20);
                translate([0,0,draw_h/2+draw_r-draw_cuth])
                    rotate([90,0,0])
                        cylinder(2*draw_d,draw_r+1,draw_r+1,center=true,$fn=60);
        }
    }
    translate([0,0,-draw_h/2+d])
    cube([draw_w,D-d,2],center=true);
}

module syskuffer() {
    skab();
    // Back plate
    translate([0,-D/2+d,0])
        cube([W,d,H],center=true);
    // Bottom plate
    translate([0,1,-H/2+d/2+.5])
        cube([W,D-2,2],center=true);
    //translate([0,0,draw_h/2])
    //    cube([9,2,1],center=true);

    for (x=[-2.5:2.5])
        translate([0,0,x*(draw_h+.3)+hdif/2])
            skuffe();
}

// Kugle
wheel_r1=2;
wheel_cut1=1.7;

// Udenom
wheel_r2=2.5;
wheel_d=2.5;
wheel_t=.3;
wheel_dh=.3;
wheel_cut2=.7;

// Stick
wheel_stick=3.5;
wheel_stickr=.5;

wheel_in=2;
wheel_h=wheel_r1+wheel_stick-1;

module inner_wheel() {
    difference() {
        sphere(wheel_r1,$fn=40);
        translate([0,0,wheel_cut1+wheel_r1/2]) 
            cube([2*wheel_r1,2*wheel_r1,wheel_r1],center=true);
        translate([0,0,-wheel_cut1-wheel_r1/2]) 
            cube([2*wheel_r1,2*wheel_r1,wheel_r1],center=true);
    }
}

module outer_wheel() {
    difference() {
        union() {
            cylinder(wheel_d,wheel_r2,wheel_r2,center=true,$fn=40);
            translate([1.2,0,0])
                rotate([90,0,0]) 
                    cylinder(wheel_stick,wheel_stickr,wheel_stickr,$fn=20);
        }
        cylinder(1.1*wheel_d,wheel_r2-wheel_t,wheel_r2-wheel_t,center=true,$fn=40);
        translate([0,2.5-wheel_cut2,0]) cube([5,5,5],center=true);
    }
}


module wheels() {
    dx=W/2-wheel_in;
    dy=D/2-wheel_in;
    for (x=[-1,1]) for (y=[-1,1]) {
        translate([x*dx,y*dy,0])
        rotate([-90,0,90]) translate([0,-wheel_r1,0]) {
            outer_wheel();
            inner_wheel();
        }
    }
}

translate([0,0,H/2+wheel_h]) syskuffer();
wheels();

