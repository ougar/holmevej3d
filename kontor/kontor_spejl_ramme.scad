H=140;
W=40;
D=3.5;
d=1.5;


difference() {
    cube([W,D,H],center=true);
    cube([W-2*d,10,H-2*d],center=true);
}