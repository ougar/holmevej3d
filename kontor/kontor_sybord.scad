use <MCAD/boxes.scad>;

D=90;
W=145;
T=4;

topr=1;
din=4;
stifh=8;
stifd=2.5;

r1=3.5/2;
r2=4.5/2;
r3=5.9/2;
h1=20;
h2=49;

dd=0.01;

dug_down=10;
dug_thick=.1;

module leg() {
    cylinder(h1,r1,r2,$fn=40);
    translate([0,0,h1-dd])
        cylinder(h2,r2,r3,$fn=40);
}

module legs() {
    dx=W/2-din-r3;
    dy=D/2-din-r3;
    for (x=[-1,1]) for (y=[-1,1]) 
        translate([x*dx,y*dy,0])
            leg();
}

module tabletop() {
    translate([0,0,h1+h2+T/2-dd]) {
        translate([0,0,-T/4]) roundedBox([W,D,T/2],topr,$fn=40);
        translate([0,0, T/4]) roundedBox([W,D,T/2],topr,$fn=40);
    }
}

module stiffers() {
    dx=W/2-din-r3;
    dy=D/2-din-r3;
    for (x=[-1,1])
        translate([x*dx,0,h1+h2-stifh/2+dd])
            cube([stifd,2*dy,stifh],center=true);
    for (y=[-1,1])
        translate([0,y*dy,h1+h2-stifh/2+dd])
            cube([2*dx,stifd,stifh],center=true);
}

module dug() {
    translate([0,0,h1+h2+T-dug_down/2+dug_thick])
        difference() {
            cube([W,D,dug_down],center=true);
            translate([0,0,-dug_thick])
                cube([W-2*dug_thick,D-2*dug_thick,dug_down],center=true);
        }

}

color("#5C482D") {
    legs();
    tabletop();
    stiffers();
}
color("yellow",alpha=.8) dug();