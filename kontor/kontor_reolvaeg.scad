H=200;
D=30;
d=2;
dify=.4;
footy=2;
footz=4;

module shelf(W,h) {
    translate([0,0,h+footz]) cube([W-d,D-dify,2]);
}

module box(W) {
    // Sight side
    cube([2,D,H]);
    // Left side
    translate([W-d,0,0]) cube([2,D,H]);
    // Back plate
    cube([W-1,2,H-1]);
    // Bottom stiffer
    translate([0,D-footy-d]) cube([W-d,2,footz+1]);
}

module reol(W,N) {
    box(W);
    shelf(W,H-footy-d-d-dify);
    for (x=N)
        shelf(W,x);
}

reol1=[0,35,70.5,96,121.5,156.5];
reol2=[0,35,70,77,,96,125,156.5];
reol3=[0,31.5,63.5,96,127.5,159.6];

space=.5;
w1=60;
w2=90;
translate([0*(w1+space),0,0]) reol(w1,reol1);
translate([1*(w1+space),0,0]) reol(w1,reol1);
translate([2*(w1+space),0,0]) reol(w1,reol2);
translate([3*(w1+space),0,0]) reol(w1,reol2);
translate([4*(w1+space),0,0]) reol(w2,reol3);