use <MCAD/boxes.scad>;

H=200;
D=40;
W=60;

dh=.2;
dw=.1;
but=2;
h1=14.5;
h2=H-4*(h1+dh)-but-dh;
lidd=2;

cube([W,D-lidd,H],center=true);

module drawer(x) {
    translate([0,0,x*(h1+dh)])
        roundedBox([W-dw,lidd,h1],.5,$fn=20);
}

translate([0,D/2,-H/2+h1/2+but]) {
    drawer(0);
    drawer(1);
    drawer(2);
    drawer(3);
}
translate([0,D/2,-H/2+h2/2+4*(h1+dh)+but])
    roundedBox([W-dw,lidd,h2],.5,$fn=20);