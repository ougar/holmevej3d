edge=1.5;
corn=1;
thick=.5;
hole_h=2;
hole_w=.5;

space_x=5;
space_y=5;

mount_r=.7;
mount_d=2.5;
mount_h=.75;

numx=8;
numy=13;

function px(x) = x*space_x/2;
function py(y) = y*space_y/2;
  
module hole(x,y) {
    translate([px(x),py(y),0]) {
        translate([0,hole_h/2-hole_w/2,0])
            cylinder(2*thick,hole_w/2,hole_w/2,$fn=20, center=true);
        translate([0,-hole_h/2+hole_w/2,0])
            cylinder(2*thick,hole_w/2,hole_w/2,$fn=20, center=true);
        cube([hole_w,hole_h-hole_w,2*thick], center=true);
    }
}

module allholes() {
    for (x=[0:numx-1])
    for (y=[0:numy-1])
        hole(2*x,2*y);
    for (x=[0:numx])
    for (y=[0:numy-2])
        hole(2*x-1,2*y+1);
}


module holeplate() {
    hull() {
        translate([-edge-space_x/2+corn-hole_w/2,-hole_h/2+corn-edge,0])
            cylinder(thick,corn,corn,$fn=20, center=true);
        translate([(numx-.5)*space_x-corn+hole_w/2+edge,-hole_h/2+corn-edge,0])
            cylinder(thick,corn,corn,$fn=20, center=true);
        translate([-edge-space_x/2+corn-hole_w/2,(numy-1)*space_y+hole_h/2-corn+edge,0])
            cylinder(thick,corn,corn,$fn=20, center=true);
        translate([(numx-.5)*space_x-corn+hole_w/2+edge,(numy-1)*space_y+hole_h/2-corn+edge,0])
            cylinder(thick,corn,corn,$fn=20, center=true);
    }
}

module mount() {
    translate([0,0,mount_h/2-mount_d/2])
        cylinder(mount_d+mount_h,mount_r,mount_r, center=true, $fn=20);
}

module mounts() {
    translate([px(0),py(2),0]) mount();
    translate([px(0),py(2*numy-4),0]) mount();
    translate([px(2*numx-2),py(2*numy-4),0]) mount();
    translate([px(2*numx-2),py(2),0]) mount();
}

difference() {
    holeplate();
    allholes();
}
mounts();



