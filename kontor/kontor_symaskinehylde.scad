D=43.5;
T=4;
W=125;
r=.7;

translate([-T/2+r,0,0]) cylinder(125,r,r,center=true,$fn=30);
translate([ T/2-r,0,0]) cylinder(125,r,r,center=true,$fn=30);
cube([T-2*r,2*r,125],center=true);
translate([0,(D-r)/2,0]) cube([T,D-r,125],center=true);
